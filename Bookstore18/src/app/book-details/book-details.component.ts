import {Component, OnInit} from '@angular/core';
import {Book} from '../shared/book';
import {Rating} from '../shared/rating';
import {BookStoreService} from "../shared/book-store.service";
import {ActivatedRoute, Router} from "@angular/router";
import {BookFactory} from "../shared/book-factory";
import {AuthService} from "../shared/authentication.service";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'bs-book-details',
  templateUrl: './book-details.component.html',
  styles: []
})
export class BookDetailsComponent implements OnInit {
    book: Book = BookFactory.empty();
    added;

    constructor(
    private bs: BookStoreService,
    private router: Router,
    private route: ActivatedRoute,
    public authService: AuthService) { }


    ngOnInit() {
        this.added = false;
        const params = this.route.snapshot.params;
        this.bs.getSingle(params['isbn']).subscribe(b => this.book = b);
    }

    getRating(ratings: Array<Rating>) {
        var length = 0;
        if(this.book.id != null && ratings.length > 0) {
            var ratingSum = 0;
            for(var rating in ratings) {
                ratingSum += ratings[rating].stars;
            }

            length = Math.round(ratingSum / ratings.length);
        }
        return new Array(length)
    }

    getEmptyStars(ratings: Array<Rating>) {
        var length = 5;
        var rated = this.getRating(ratings);
        length = length - rated.length;
        return new Array(length);
    }

    removeBook() {
        if (confirm('Buch wirklich löschen?')) {
            this.bs.remove(this.book.isbn)
            .subscribe(res => this.router.navigate(['../'], { relativeTo: this.route }));
        }
    }

    addToShoppingCart() {
        var storedBooks = [];
        if(!isNullOrUndefined(localStorage.getItem('shoppingcart'))) {
            storedBooks = JSON.parse(localStorage.getItem('shoppingcart'));
        }
        storedBooks.push(this.book);
        localStorage.setItem('shoppingcart', JSON.stringify(storedBooks));
        this.added = true;
    }

    addedToShoppingCart(){
        return this.added;
    }
}
