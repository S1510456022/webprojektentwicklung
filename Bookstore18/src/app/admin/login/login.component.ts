import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from "../../shared/authentication.service";
import {Order} from "../../shared/order";
import {BookStoreService} from "../../shared/book-store.service";


interface Response {
  response: string;
  result: {
    token: string;
  };
}

@Component({
  selector: 'bs-account',
  templateUrl: './login.component.html'
})
export class AccountComponent implements OnInit {
  orders: Order[];
  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private router: Router,
              private authService: AuthService, private bs: BookStoreService ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });

    this.bs.getAllOrders(Number(this.authService.getUserId())).subscribe(res => this.orders = res);
  }

  login() {
    const val = this.loginForm.value;
    if (val.username && val.password) {
      this.authService.login(val.username, val.password).subscribe(res => {
        const resObj = res as Response;
        if (resObj.response === "success") {
          this.authService.setLocalStorage(resObj.result.token);
          this.router.navigateByUrl('/');
        }
      });
    }
  }

  isLoggedIn(){
    return this.authService.isLoggedIn();
  }

  logout(){
    this.authService.logout();
  }
}
