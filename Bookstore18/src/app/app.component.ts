import { Component } from '@angular/core';
import {AuthService} from "./shared/authentication.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'bs-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  constructor(private authService: AuthService, private route: ActivatedRoute) {  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  getLoginLabel(){
    if(this.isLoggedIn()){
      return "Mein Konto";
    } else {
      return "Login";
    }
  }

}
