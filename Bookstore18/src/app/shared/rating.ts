export class Rating {
    constructor(public id: number, public stars: number, public comment: string, public user_id: number, public username: string, public book_id: number) {
    }

}
