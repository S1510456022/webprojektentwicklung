export class ShopUser {
    constructor(public id: number, public name: string, public email: string, public password: string, public typeOfUser: string,
                public firstName: string, public lastName: string, public street: string, public postalcode: number, public city: string) {
    }

}
