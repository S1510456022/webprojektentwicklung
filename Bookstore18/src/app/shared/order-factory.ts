import { Order } from './order';

export class OrderFactory {

    static empty(): Order {
        return new Order(null, 0, 0, 0, 0, []);
    }
}
