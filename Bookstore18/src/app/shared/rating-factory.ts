import { Rating } from './rating';

export class RatingFactory {

    static empty(): Rating {
        return new Rating(null, 0, '', 0, '', 0);
    }

    static fromObject(rawRating: any): Rating {
        return new Rating(
            rawRating.id,
            rawRating.stars,
            rawRating.comment,
            rawRating.user_id,
            rawRating.username,
            rawRating.book_id
        );
    }
}
