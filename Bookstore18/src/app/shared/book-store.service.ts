import { Injectable } from '@angular/core';
import {Book, Author} from "./book";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Rating} from "./rating";
import {Order} from "./order";

@Injectable()
export class BookStoreService {
  private api = 'http://bookstore-rest.s1510456022.student.kwmhgb.at/api';

  constructor(private http: HttpClient) { }

  getAll(): Observable<Array<Book>> {
    return this.http.get(`${this.api}/books`)
      .retry(3).catch(this.errorHandler);
  }

  getSingle(isbn: string): Observable<Book> {
    console.log("in get single")
    return this.http.get<Book>(`${this.api}/book/${isbn}`)
      .retry(3).catch(this.errorHandler);
  }

  create(book: Book): Observable<any> {
    return this.http.post(`${this.api}/book`, book)
      .catch(this.errorHandler);
  }

  update(book: Book): Observable<any> {
    return this.http.put(`${this.api}/book/${book.isbn}`, book)
      .catch(this.errorHandler);
  }

  remove(isbn: string): Observable<any> {
    return this.http.delete(`${this.api}/book/${isbn}`)
      .catch(this.errorHandler);
  }

  getAllSearch(searchTerm: string): Observable<Array<Book>> {
    return this.http
      .get<Book>(`${this.api}/books/search/${searchTerm}`)
      .retry(3).catch(this.errorHandler);
  }

  check(isbn: string): Observable<Boolean> {
    return this.http.get<Boolean>(`${this.api}/book/checkisbn/${isbn}`).catch(this.errorHandler);
  }

  getAllAuthors(): Observable<Array<Author>> {
      return this.http.get(`${this.api}/authors`)
          .retry(3).catch(this.errorHandler);
  }

  createOrder(order: Order): Observable<any> {
      return this.http.post(`${this.api}/order`, order).catch(this.errorHandler);
  }

  getAllOrders(user_id: number): Observable<Array<Order>> {
      return this.http.get(`${this.api}/orders/${user_id}`).retry(3).catch(this.errorHandler);
  }

  createRating(rating: Rating): Observable<any> {
    return this.http.post(`${this.api}/rating`, rating).catch(this.errorHandler);
  }

  private errorHandler(error: Error | any): Observable<any> {
    return Observable.throw(error);
  }
}
