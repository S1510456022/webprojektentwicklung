import {Injectable} from '@angular/core';
import {isNullOrUndefined} from "util";
import {HttpClient} from "@angular/common/http";
import * as decode from 'jwt-decode';

//npm install --save-dev jwt-decode

@Injectable()
export class AuthService {

  private api:string = 'http://bookstore-rest.s1510456022.student.kwmhgb.at/api/auth';//'http://localhost:8080/api/auth';

  constructor(private http: HttpClient) {
  }

  login(email: string, password: string ) {
    return this.http.post(`${this.api}/login`, {'email': email, 'password': password});
  }

  public getCurrentUserId(){
    return Number.parseInt(localStorage.getItem('userId'));
  }

  public setLocalStorage(token: string) {
    const decodedToken = decode(token);
    localStorage.setItem('token', token);
    localStorage.setItem('userId', decodedToken.user.id);
    localStorage.setItem('username', decodedToken.user.username);
    localStorage.setItem('typeOfUser', decodedToken.user.type);

    var user = {
        'id': decodedToken.user.id,
        'username': decodedToken.user.username,
        'typeOfUser': decodedToken.user.type,
        'firstname': decodedToken.user.firstname,
        'lastname': decodedToken.user.lastname,
        'street': decodedToken.user.street,
        'postalcode': decodedToken.user.postalcode,
        'city': decodedToken.user.city
    };

    localStorage.setItem('user', JSON.stringify(user));
  }

  logout() {
    this.http.post(`${this.api}/logout`, {});
    localStorage.removeItem("token");
    localStorage.removeItem("userId");
    localStorage.removeItem("username");
    localStorage.removeItem('typeOfUser');
  }

  public isLoggedIn() {
    return !isNullOrUndefined(localStorage.getItem("token"));
  }

  public isShopUser() {
    return localStorage.getItem('typeOfUser') == 'shopuser';
  }

  public isAdmin() {
    return localStorage.getItem('typeOfUser') == 'admin';
  }

  public getUser() {
    return JSON.parse(localStorage.getItem('user'));
  }

  public getUserId() {
    return localStorage.getItem('userId');
  }

  public getUserName() {
    return localStorage.getItem('username');
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

}
