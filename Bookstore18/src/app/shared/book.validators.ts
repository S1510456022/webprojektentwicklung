import {Observable} from 'rxjs/Observable';
import {FormControl} from '@angular/forms';
import {BookStoreService} from "./book-store.service";
import 'rxjs/add/operator/map';

export class BookValidators {
    static isbnFormat(control: FormControl): {[error: string]: any} {
        if(!control.value) {
            return null
        }
        const isolatedNumbers = control.value.replace(/-/g, '');
        const isbnPattern = /(^\d{10}$)|(^\d{13}$)/;
        return isbnPattern.test(isolatedNumbers) ? null : {isbnFormat: {valid: false}};
    }

    static isbnExists(bs : BookStoreService) {
        return function(control: FormControl): Observable<{[error: string]: any}> {
            return bs.check(control.value).map(exists => !exists ? null : {isbnExists : {valid: false}});
        }
    }
}
