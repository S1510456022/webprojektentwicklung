import { Image } from './image';
export { Image } from './image';
import { Author } from './author';
export { Author } from './author';
import { Rating } from './rating';
export { Rating } from './rating';
import { ShopUser } from './shopuser';
export { ShopUser } from './shopuser';
import { Order } from './order';
export { Order } from './order';

export class Book {
  constructor (
    public id: number,
    public isbn: string,
    public title: string,
    public authors: Author[],
    public published: Date,
    public user_id: number,
    public price: number,
    public ratings?: Rating[],
    public shopuser?: ShopUser[],
    public order?: Order[],
    public subtitle?: string,
    public images?: Image[],
    public description?: string
  ) {  }
}
