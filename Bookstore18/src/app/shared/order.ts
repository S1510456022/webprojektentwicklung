export class Order {
    constructor(public id: number, public totalprice: number, public nettoprice: number, public bruttoprice: number, public user_id: number, public books: string[]) {
    }

}
