import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'bs-book-order-item',
  templateUrl: './book-order-item.component.html',
  styles: []
})
export class BookOrderItemComponent implements OnInit {
    amount = {};
    @Input() books = [];

    constructor(private route: ActivatedRoute) { }

    ngOnInit() {
        if(!this.isAccount()) {
            for(var book in this.books) {
                var curBook = this.books[book];
                this.amount[curBook.id] = 1;
                for(var i = 0; i < this.books.length; i++) {
                    if(curBook != this.books[i]) {
                        if(curBook.id == this.books[i].id) {
                            this.amount[curBook.id]++;
                            this.books.splice(i, 1);
                            i--;
                        }
                    }
                }
            }
        } else {
            for(var book in this.books) {
                this.books[book].price = this.books[book].pivot.price;
                this.amount[this.books[book].id] = this.books[book].pivot.amount;
            }
        }
    }

    getBruttoPrice() {
        var price = 0;
        for (var book in this.books) {
            price += (this.books[book].price * this.amount[this.books[book].id]);
        }
        return price.toFixed(2);
    }

    getNettoPrice() {
        return (Number(this.getBruttoPrice()) / 1.1).toFixed(2);
    }

    getPrice() {
        return (Number(this.getBruttoPrice()) + 5.95).toFixed(2);
    }

    getBookPrice(book) {
        return (book.price * this.amount[book.id]).toFixed(2);
    }

    getAmount(id) {
        return this.amount[id];
    }

    addAmount(id, book) {
        this.amount[id]++;
        var storedBooks = JSON.parse(localStorage.getItem('shoppingcart'));
        storedBooks.push(book);
        localStorage.setItem('shoppingcart', JSON.stringify(storedBooks));
    }

    removeAmount(id, book) {
        if(this.amount[id]>0) {
            this.amount[id]--;
            var storedBooks = JSON.parse(localStorage.getItem('shoppingcart'));

            for(var b in storedBooks) {
                if(book.id == storedBooks[b].id) {
                    storedBooks.splice(Number(b), 1);
                    break;
                }
            }
            localStorage.setItem('shoppingcart', JSON.stringify(storedBooks));
        }
    }

    isShoppingCart(){
        const path = this.route.snapshot.routeConfig.path;
        return path == 'shoppingcart';
    }

    isAccount(){
        const path = this.route.snapshot.routeConfig.path;
        return path == 'account';
    }
}
