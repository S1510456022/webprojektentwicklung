import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { BookFormErrorMessages } from './book-form-error-messages';
import {BookFactory} from "../shared/book-factory";
import {BookStoreService} from "../shared/book-store.service";
import {BookValidators} from "../shared/book.validators";
import {Book} from "../shared/book";
import {Author} from "../shared/author";
import {AuthService} from "../shared/authentication.service";


@Component({
  selector: 'bs-book-form',
  templateUrl: './book-form.component.html'
})
export class BookFormComponent implements OnInit {
  myForm: FormGroup;
  book = BookFactory.empty();
  errors: { [key: string]: string } = {};
  isUpdatingBook = false;
  thumbnails: FormArray;
  allAuthors: Author[];

  constructor(private fb: FormBuilder, private bs: BookStoreService,
              private route: ActivatedRoute, private router: Router, private authService: AuthService) { }

  ngOnInit() {
    const isbn = this.route.snapshot.params['isbn'];
    this.bs.getAllAuthors().subscribe(res => this.allAuthors = res);

    if (isbn) {
    this.isUpdatingBook = true;
    this.bs.getSingle(isbn).subscribe(book => {
        this.book = book;
        this.initBook();
      });
    }
    this.initBook();
  }

  initBook() {
    this.buildThumbnailsArray();
    this.myForm = this.fb.group({
      id: this.book.id,
      title: [this.book.title, Validators.required],
      subtitle: this.book.subtitle,
      isbn: [this.book.isbn, [
        Validators.required,
        BookValidators.isbnFormat
      ], this.isUpdatingBook ? null : BookValidators.isbnExists(this.bs)],
      description: this.book.description,
      authors: [this.book.authors, Validators.required],
      thumbnails: this.thumbnails,
      published: [new Date(this.book.published), Validators.required],
      price: [this.book.price, Validators.required]
    });

    this.myForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  buildThumbnailsArray() {
    if(this.book.images.length == 0) {
        this.book.images = [{id: 0, url: '', title: ''}];
    }

    this.thumbnails = this.fb.array(
      this.book.images.map(
        t => this.fb.group({
          id: this.fb.control(t.id),
          url: this.fb.control(t.url,[Validators.required]),
          title: this.fb.control(t.title)
        })
      )
    );
  }

  addThumbnailControl() {
    this.thumbnails.push(this.fb.group({ url: null, title: null }));
  }

  removeThumbnailControl(index) {
    this.thumbnails.removeAt(index);
  }

  getAuthors() {
    return this.book.authors;
  }

  getFreeAuthors() {
    var freeAuthors = [];
    for(var author in this.allAuthors) {
      var curAuthor = this.allAuthors[author];
      var exists = false;
      for(var auth in this.book.authors) {
        if(curAuthor.id == this.book.authors[auth].id) {
          exists = true;
        }
      }
      if(!exists) {
        freeAuthors.push(curAuthor);
      }
    }
    return freeAuthors;
  }

  removeAuthor(index){
    this.book.authors.splice(index, 1);
    this.updateErrorMessages();
  }

  addAuthor(author: Author){
    this.book.authors.push(author);
    this.errors.authors = undefined;
    this.updateErrorMessages();
  }


  submitForm() {
    this.myForm.value.thumbnails = this.myForm.value.thumbnails.filter(thumbnail => thumbnail.url);
    const book: Book = BookFactory.fromObject(this.myForm.value);
    book.user_id = Number(this.authService.getUserId());
    book.authors = this.book.authors;

    if (this.isUpdatingBook) {
      this.bs.update(book).subscribe(res => {
        this.router.navigate(['../../books', book.isbn], { relativeTo: this.route });
      });
    } else {
      this.bs.create(book).subscribe(res => {
        this.book = BookFactory.empty();
        this.myForm.reset(BookFactory.empty());
      });
    }
  }

  updateErrorMessages() {
    this.errors = {};

    if(this.book.authors.length == 0) {
      this.errors.authors = "Es muss mindestens 1 Autor angegeben werden!";
    }

    for (const message of BookFormErrorMessages) {
      const control = this.myForm.get(message.forControl);
      if (control &&
        control.dirty &&
        control.invalid &&
        control.errors[message.forValidator] &&
        !this.errors[message.forControl]) {
          this.errors[message.forControl] = message.text;
      }
    }
  }
}
