import {Component, OnInit} from '@angular/core';
import {Book} from "../shared/book";
import {AuthService} from "../shared/authentication.service";

@Component({
  selector: 'shopping-cart',
  templateUrl: './shopping-cart.component.html'
})

export class ShoppingCartComponent implements OnInit {
    books: Book[];
    constructor(private authService: AuthService) { }

    ngOnInit(){
        if(localStorage.getItem('shoppingcart')) {
            this.books = JSON.parse(localStorage.getItem('shoppingcart'));
        }
    }
}
