import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { BookDetailsComponent } from './book-details/book-details.component';
import { BookListComponent } from './book-list/book-list.component';
import { HomeComponent } from './home/home.component';
import {BookFormComponent} from "./book-form/book-form.component";
import {AccountComponent} from "./admin/login/login.component";
import {BookRatingsComponent} from "./book-ratings/book-ratings.component";
import {ShoppingCartComponent} from "./shopping-cart/shopping-cart.component";
import {OrderComponent} from "./order/order.component";

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'books', component: BookListComponent },
  { path: 'books/:isbn', component: BookDetailsComponent },
  { path: 'books/:isbn/ratings', component: BookRatingsComponent },
  { path: 'admin', component: BookFormComponent },
  { path: 'admin/:isbn', component: BookFormComponent },
  { path: 'account', component: AccountComponent, pathMatch: 'full'},
  { path: 'shoppingcart', component: ShoppingCartComponent },
  { path: 'order', component: OrderComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
