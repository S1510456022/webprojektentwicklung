export class ErrorMessage {
    constructor(
        public forControl: string,
        public forValidator: string,
        public text: string
    ) { }
}
export const RatingFormErrorMessages = [
    new ErrorMessage('comment', 'required', 'Ein Kommentar muss angegeben werden')
];
