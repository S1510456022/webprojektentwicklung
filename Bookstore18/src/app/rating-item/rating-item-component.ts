import {Component, Input, OnInit} from '@angular/core';
import {Book} from "../shared/book";
import {BookStoreService} from "../shared/book-store.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Rating} from "../shared/rating";
import {RatingFactory} from "../shared/rating-factory";
import {RatingFormErrorMessages} from "./rating-form-error-messages";
import {AuthService} from "../shared/authentication.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'bs-rating-item',
  templateUrl: './rating-item-component.html',
  styles: []
})
export class RatingItemComponent implements OnInit {
    myForm: FormGroup;
    rating: Rating = RatingFactory.empty();
    errors: { [key: string]: string } = {};

    @Input() book: Book;

    constructor( private bs: BookStoreService, private route: ActivatedRoute, private router: Router,
                private fb: FormBuilder, private authService: AuthService) { }

    ngOnInit() {
        this.initRating();
    }

    initRating(){
        this.myForm = this.fb.group({
            stars: this.rating.stars,
            comment: [this.rating.comment, Validators.required],
            user_id: this.rating.user_id,
            username: this.rating.username,
            book_id: this.rating.book_id
        });
        this.myForm.statusChanges.subscribe(() => this.updateErrorMessages());
    }

    submitRating() {
        const rating: Rating = RatingFactory.fromObject(this.myForm.value);
        rating.user_id = Number(this.authService.getUserId());
        rating.username = this.authService.getUserName();
        rating.book_id = Number(this.book.id);
        rating.stars = this.rating.stars;

        this.bs.createRating(rating).subscribe(res => {
            this.rating = RatingFactory.empty();
            this.myForm.reset(RatingFactory.empty());
            this.emptyStar(-1);
            console.log(this.route);
            if(this.route.routeConfig.path == 'books/:isbn') {
                this.router.navigate(['ratings'], { relativeTo: this.route });
            }
        });
    }

    fillStar(index){
        var stars = document.getElementsByClassName('ratingStar');
        for(var star in stars) {
            if(star <= index) {
                stars[star].classList.remove('outline');
                stars[star].classList.add('yellow')
            }
        }
    }

    emptyStar(index){
        var stars = document.getElementsByClassName('ratingStar');
        for(var star in stars) {
            if(star > index) {
                stars[star].classList.remove('yellow');
                stars[star].classList.add('outline')
            }
        }
    }

    rateBook(rating) {
        var prevRating = this.rating.stars;
        this.rating.stars = rating;

        if(prevRating<rating){
            this.fillStar(rating-1);
        } else {
            this.emptyStar(rating-1);
        }

        this.updateErrorMessages();
    }

    updateErrorMessages() {
        this.errors = {};

        if(!this.rating.stars) {
            this.errors.stars = "Es muss ein Wert angegeben werden!";
        }

        for (const message of RatingFormErrorMessages) {
            const control = this.myForm.get(message.forControl);
            if (control &&
                control.dirty &&
                control.invalid &&
                control.errors[message.forValidator] &&
                !this.errors[message.forControl]) {
                this.errors[message.forControl] = message.text;
            }
        }
    }

}
