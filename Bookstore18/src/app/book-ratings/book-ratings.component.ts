import {Component, OnInit } from '@angular/core';
import {Book} from "../shared/book";
import {BookStoreService} from "../shared/book-store.service";
import {BookFactory} from "../shared/book-factory";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../shared/authentication.service";

@Component({
  selector: 'bs-book-ratings',
  templateUrl: './book-ratings.component.html'
})
export class BookRatingsComponent implements OnInit {
  book: Book = BookFactory.empty();

  constructor(private bs: BookStoreService,  private router: Router,
              private route: ActivatedRoute, private authService: AuthService){ }

  ngOnInit() {
    const params = this.route.snapshot.params;
    this.bs.getSingle(params['isbn']).subscribe(b => this.book = b);
  }

  getRatings(){
    return this.book.ratings;
  }

  getFullStars(number) {
    return new Array(number);
  }

  getEmptyStars(number) {
      return new Array(5 - number);
  }

  getUser(rating) {
    return rating.username;
  }
}
