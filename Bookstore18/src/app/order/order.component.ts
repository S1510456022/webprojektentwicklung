import {Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../shared/authentication.service";
import {Book} from "../shared/book";
import {ShopUser} from "../shared/shopuser"
import {BookStoreService} from "../shared/book-store.service";
import {Order} from "../shared/order";
import {OrderFactory} from "../shared/order-factory";

@Component({
  selector: 'bs-order',
  templateUrl: './order.component.html'
})

export class OrderComponent implements OnInit {
    books: Book[];
    user: ShopUser;
    amount = {};

    constructor(private router: Router, private route: ActivatedRoute,
                private authService: AuthService, private bs: BookStoreService) { }

    ngOnInit(){
        this.user = this.authService.getUser();
        this.books = JSON.parse(localStorage.getItem('shoppingcart'));

        for(var book in this.books) {
            var curBook = this.books[book];
            this.amount[curBook.id] = 1;
            for(var i = 0; i < this.books.length; i++) {
                if(curBook != this.books[i]) {
                    if(curBook.id == this.books[i].id) {
                        this.amount[curBook.id]++;
                    }
                }
            }
        }
    }

    getBruttoPrice() {
        var price = 0;
        for (var book in this.books) {
            price += (this.books[book].price * this.amount[this.books[book].id]);
        }
        return price;
    }

    getNettoPrice() {
        return (this.getBruttoPrice() / 1.1).toFixed(2);
    }

    getPrice() {
        return (this.getBruttoPrice() + 5.95).toFixed(2);
    }

    placeOrder() {
        const order: Order = OrderFactory.empty();
        order.totalprice = Number(this.getPrice());
        order.nettoprice = Number(this.getNettoPrice());
        order.bruttoprice = Number(this.getBruttoPrice());
        order.user_id = Number(this.user.id);

        this.books = JSON.parse(localStorage.getItem('shoppingcart'));
        for(var book in this.books) {
            order.books.push(this.books[book].isbn);
        }

        this.bs.createOrder(order).subscribe(res => {
            localStorage.removeItem('shoppingcart');
            this.router.navigate(['../account']);
        });
    }
}
