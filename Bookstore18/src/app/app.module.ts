import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookListItemComponent } from './book-list-item/book-list-item.component';
import { BookOrderItemComponent } from "./book-order-item/book-order-item.component";
import { BookDetailsComponent } from './book-details/book-details.component';
import { BookRatingsComponent } from "./book-ratings/book-ratings.component";
import {BookStoreService} from "./shared/book-store.service";
import { HomeComponent } from './home/home.component';
import {AppRoutingModule} from "./app-routing.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { SearchComponent } from './search/search.component';
import { BookFormComponent } from './book-form/book-form.component';
import { ReactiveFormsModule} from "@angular/forms";
import {DateValueAccessorModule} from "angular-date-value-accessor";
import {AccountComponent} from './admin/login/login.component';
import {AuthService} from "./shared/authentication.service";
import {TokenInterceptorService} from "./shared/token-interceptor.service";
import {JwtInterceptorService} from "./shared/jwt-interceptor.service";
import {ShoppingCartComponent} from "./shopping-cart/shopping-cart.component";
import {OrderComponent} from "./order/order.component";
import {RatingItemComponent} from "./rating-item/rating-item-component";


@NgModule({
  declarations: [
    AppComponent,
    BookListComponent,
    BookOrderItemComponent,
      RatingItemComponent,
    BookListItemComponent,
    BookDetailsComponent,
    BookRatingsComponent,
    ShoppingCartComponent,
    HomeComponent,
    SearchComponent,
    BookFormComponent,
    AccountComponent,
    OrderComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule, HttpClientModule,
    ReactiveFormsModule, DateValueAccessorModule
  ],
  providers: [BookStoreService,AuthService,
      {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptorService,
        multi: true
      },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptorService,
      multi: true
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
